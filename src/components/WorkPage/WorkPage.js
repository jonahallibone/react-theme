import React, { Component } from 'react';
import '../../App.css';

class WorkPage extends Component {
  render() {
    return (
      <section className="transition-page work-page">
        <h2>Work Page</h2>
      </section>
    );
  }
}

export default WorkPage;
